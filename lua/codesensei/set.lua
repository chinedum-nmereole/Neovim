local opt = vim.opt

vim.g.astro_typescript = 'enable'

-- number line
opt.nu = true
opt.rnu = true

--tab space
opt.tabstop = 5
opt.softtabstop = 5
opt.shiftwidth = 5
opt.expandtab = true
opt.textwidth = 120
opt.formatoptions:append("t")


opt.termguicolors = true

--Set lualine as statusline
require('lualine').setup{
     options = {
          icons_enabled = true,
          theme = 'catppuccin',
          component_separators = '|',
          section_separators = '',
     },
}

--Set bufferline
require('bufferline').setup{
    options = {
         diagnostics = true,
         --separator_style = 'padded_slant',
         numbers = "ordinal",
    },
    
}

opt.updatetime = 50

opt.colorcolumn = "170"
opt.wrap = false



opt.list = true
--opt.listchars:append "eol:↴"

require("indent_blankline").setup {
    char = '¦',
    show_end_of_line = true,
}



opt.clipboard = 'unnamedplus'



