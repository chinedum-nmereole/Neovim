local lsp = require('lsp-zero')
lsp.preset('recommended')

lsp.ensure_installed({
	'eslint',
	'rust_analyzer',
	'clangd',
	'gopls',
     'pyright',

})

lsp.set_preferences({
     'sign_icons'
})


lsp.configure('gopls', {
     cmd = {'gopls'},
     filetypes = {"go", "gomod", "gowork", "gotmpl"},
     settings = {
          gopls = {
               completeUnimported = true,
               experimentalPostfixCompletions = true,
               buildFlags =  {"-tags=integration"},
               --usePlaceholder = true,
               analyses = {
                    unusedparams = true,
               },
          },
     },
     on_attach = on_attach,
})

lsp.setup()


local cmp = require('cmp')
cmp.setup({
     snippet = {
          expand = function (args)
               require('luasnip').lsp_expand(args.body)
          end
     },
     sources = cmp.config.sources({
          {name = 'nvim-lsp' },
          {name = 'buffer' },
     })
})

local cmp_action = require('lsp-zero').cmp_action()

require('luasnip.loaders.from_vscode').lazy_load()

cmp.setup({
     sources = {
          {name = 'path'},
          {name = 'nvim_lsp'},
          {name = 'buffer', keyword_length = 3},
          {name = 'luasnip', keyword_length = 2},
     },
})





vim.diagnostic.config({
  virtual_text = true,
  signs = true,
  underline = true,
  update_in_insert = true,
  severity_sort = true,
  virtual_lines = false,
})

-- Show line diagnostics automatically in hover window
vim.o.updatetime = 100 
vim.cmd [[autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=true})]]

--local cmp_select = {behavior = cmp.SelectBehavior.Select}
-- To learn how to configure a language server
-- see :help lsp-zero.configure()


